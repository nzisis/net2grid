<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 3/9/2020
 * Time: 12:19 PM
 */
namespace Model;


class Data
{
    /**
     * @var string
     */
    public $gatewayEui;

    /**
     * @var string
     */
    public $profileId;


    /**
     * @var string
     */
    public $endpointId;

    /**
     * @var string
     */
    public $clusterId;

    /**
     * @var string
     */
    public $attributeId;


    /**
     * @var double
     */
    public $value;

    /**
     * @var double
     */
    public $timestamp;

}