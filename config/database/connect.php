<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 3/10/2020
 * Time: 11:49 AM
 */


$array = explode("\n", file_get_contents('..\config\database\credentials.txt'));

$host = explode(":", $array[0])[1];
$dbName = explode(":", $array[1])[1];
$user = explode(":", $array[2])[1];
$password = explode(":", $array[3])[1];
$port = explode(":", $array[4])[1];

$connection_string = "host=$host port=$port dbname=$dbName user=$user password=$password";

$db_connection = pg_connect($connection_string);
//if ($db_connection) echo "Successful Connection" . "<br />";
//else echo "Something went wrong bro";