<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 3/9/2020
 * Time: 3:38 PM
 */
require_once  '../vendor/autoload.php';

use PhpAmqpLib\Connection\AMQPStreamConnection;

$array = explode("\n", file_get_contents('..\config\rabbit\credentials.txt'));

$host = explode(":", $array[0])[1];
$user = explode(":", $array[1])[1];
$password = explode(":", $array[2])[1];
$exchange = explode(":", $array[3])[1];
$port = explode(":", $array[4])[1];
$queue = explode(":", $array[5])[1];

//$connection = new AMQPStreamConnection('localhost', 5672, 'guest', 'guest');
$connection = new AMQPStreamConnection(trim($host), $port, trim($user) , trim($password));
$channel = $connection->channel();

$channel->exchange_declare(trim($exchange), 'fanout', false, true, false);
$channel->queue_declare(trim($queue), false, true, false, false);
$channel->queue_bind(trim($queue), trim($exchange));