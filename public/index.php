<?php

require  dirname(__DIR__).'/vendor/autoload.php';
require dirname(__DIR__).'/config/bootstrap.php';
include '../config/rabbit/connect.php';


use App\Kernel;
use Symfony\Component\ErrorHandler\Debug;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpClient\HttpClient;
use Model\Data;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

$mapper = new JsonMapper();


$comsumerDataAPI = "https://j8a44btxhb.execute-api.eu-west-1.amazonaws.com/dev/results";

//Create a get http request with Symfony Http-Client
$httpClient = HttpClient::create();
$response = $httpClient->request('GET', $comsumerDataAPI);

//Consume the data from the API
$content = $response->getContent();
echo $content . "\n";
//Convert the JSON Data that we received into a PHP object (class Data) for better handling.
$data =  $mapper->map(json_decode($content),new Data());

//Find the decimal representation of fields that we need
$gatewayEUI = hexdec($data->gatewayEui);
$profileID = hexdec($data->profileId);
$endPointID = hexdec($data->endpointId);
$clusterID = hexdec($data->clusterId);
$attributeID = hexdec($data->attributeId);

//Remove the dot from gatewayEUI
$gatewayEUI = str_replace('.','', $gatewayEUI);

//echo "|gateWay = " . $data->gatewayEui . " convertedNumber = " . $gatewayEUI;
//echo "|profileID = " . $data->profileId . " profileID = " . $profileID;
//echo "|endpointId = " . $data->endpointId . " endpointId = " . $endPointID;
//echo "|attributeId = " . $data->attributeId . " attributeId = " . $attributeID;

//Construct the routing key and the body of the message
$routingKey = $gatewayEUI . "." . $profileID . "." . $endPointID . "." . $clusterID . "." . $attributeID;
$body = $data->value . "-" . $data->timestamp;
//echo "routingKey = " . $routingKey;
//echo "body = " . $body;

//Publish the message to rabbitMQ
$msg = new AMQPMessage($body);
$channel->basic_publish($msg, trim($exchange),$routingKey);

$channel->close();
$connection->close();

if ($_SERVER['APP_DEBUG']) {
    umask(0000);

    Debug::enable();
}

$kernel = new Kernel($_SERVER['APP_ENV'], (bool) $_SERVER['APP_DEBUG']);
