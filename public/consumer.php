<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 3/10/2020
 * Time: 10:10 AM
 */

require  dirname(__DIR__).'/vendor/autoload.php';
include '../config/rabbit/connect.php';
include '../config/database/connect.php';


$callback = function ($msg) {
    $array = explode("\n", file_get_contents('..\config\database\credentials.txt'));

    $host = explode(":", $array[0])[1];
    $dbName = explode(":", $array[1])[1];
    $user = explode(":", $array[2])[1];
    $password = explode(":", $array[3])[1];
    $port = explode(":", $array[4])[1];

    $connection_string = "host=$host port=$port dbname=$dbName user=$user password=$password";

    $db_connection = pg_connect($connection_string);

    //Get the message that contains value + timestamp
    $message = $msg->body;
    $messageTokens = explode("-",$message);
    $value = doubleval($messageTokens[0]);
    $timestamp = doubleval($messageTokens[1]);

    //Get the rest values from the routing key
    $routingKey = $msg->delivery_info['routing_key'];
    $routingKeyTokens = explode(".",$routingKey);

    $gatewayEui = doubleval($routingKeyTokens[0]);
    $profileID = doubleval($routingKeyTokens[1]);
    $gendpointID = doubleval($routingKeyTokens[2]);
    $clusterID = doubleval($routingKeyTokens[3]);
    $attributeID = doubleval($routingKeyTokens[4]);

    //Insert the values into POSTGRES
    $sql = "INSERT INTO public." . '"MessageData"' . " (gateway_eui,profile_id,endpoint_id,cluster_id,attribute_id,value,timestamp) VALUES($gatewayEui,$profileID,$gendpointID,$clusterID,$attributeID,$value,$timestamp)";
    $result = pg_query($db_connection, $sql);
};

$channel->basic_consume(trim($queue), '', false, true, false, false, $callback );


while ($channel->is_consuming()) {
    $channel->wait();
}

$channel->close();
$connection->close();

